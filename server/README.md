# TNP Portal Backend and REST API

Portal for TNP Cell of IIIT Raichur.

## Structure

- api - REST API
- src
  - database - interaction with db
  - user - new user and session management

## Configurations
- config
  - .env	 (ENV - See sample.env)
  - service.json (Firebase service account)

## Local Development

### REQUIREMENTS
(RAW)
- NodeJS
- [dotenv](https://www.npmjs.com/package/dotenv-cli)
- Postgres
- Redis

(Docker and Docker Compose)
- [Docker](https://www.docker.com/)
- Docker Compose

> Note: use docker for database (for ease)

### Setting Up (RAW)

1. Clone repo
2. Install packages ```yarn install```
3. Create database `tnp_portal` in postgres (Use psql or anything of choice)
4. Enable expiry notifications in redis
   - `config set notify-keyspace-events Ex`
5. Setup `config/.env`
   - correct db url
   - correct redis url
   - enable database sync (Set `TNP_PORTAL_SYNC_DB` to `true`; at least for first time)
6. Start development server using `yarn dev`

### Setting Up (Docker)

> This is a production configuration, image should be built every time on changes in codebase.

1. Clone repo
2. Run `docker-compose up -d`

> If there are changes in files (or first time) build the image again using `docker-compose build`.