const Express = require("express");
const nodemailer = require("nodemailer");

const {
  TNP_PORTAL_SMTP_EMAIL,
  TNP_PORTAL_SMTP_PASSWORD,
  TNP_PORTAL_VERIFY_EMAIL,
} = process.env;

var transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true,
  ignoreTLS: true,
  service: "gmail",
  auth: {
    user: TNP_PORTAL_SMTP_EMAIL,
    pass: TNP_PORTAL_SMTP_PASSWORD,
  },
});

function sendVerificationEmail(
  /** @type {Express.Request} */ req,
  /** @type {Express.Response} */ res,
  /** @type {Express.NextFunction} */ next
) {
  if (TNP_PORTAL_VERIFY_EMAIL != "true") return next();

  let verificationUrl = `${req.protocol}://${req.hostname}/users/verify/`;
  if (req.hostname == "localhost")
    verificationUrl = `http://localhost:${process.env.PORT}/users/verify/`;

  var mailOptions = {
    from: TNP_PORTAL_SMTP_EMAIL,
    to: req.user.email,
    subject: "verify your account for TNP Portal",
    text: `Hello,\n\n
    Welcome to your TNP account.
    To verify your email address click on the following link:
    \n\n${verificationUrl}${req.user.id}\n\nThanks,\nTNP Portal Team`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    }
  });
  next();
}

const passwordChangeEmail = (
  /** @type {String} */ url,
  /** @type {String} */ email
) => {
  const mailOptions = {
    text: ` Here's the link to change your password: ${String(url)}`,
    to: String(email),
    subject: "Password change request",
  };
  return transporter.sendMail(mailOptions, function (error, _info) {
    if (error) {
      console.error("error sending email: ", error);
    }
  });
};

module.exports = { passwordChangeEmail, sendVerificationEmail };
